// Class 05 - Ex 2: Product CRUD

let p1 = document.querySelector('P');

// add anchor tag with cta class
let a1=document.createElement('a');
a1.setAttribute('class','cta')
let n1=document.createTextNode('Buy Now');
a1.appendChild(n1);
p1.appendChild(a1);

// Access (read) the data-color attribute of the <img>,
// log to the consoled
let i1 =document.getElementsByTagName('IMG')[0];
let val = i1.dataset.color;
console.log(val);


// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"
document.querySelectorAll('li')[2].classList.add('highlight');

// > Remove (delete) the last paragraph ("Available for purchase now")
let p = document.getElementsByTagName('main')[0].querySelector('p');
p.parentNode.removeChild(p);



