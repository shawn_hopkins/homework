// Class 05 - Ex 1: Traversing the DOM  

// given body get main
let body = document.getElementsByTagName('body')[0];
console.log(body.children[0].tagName);

// given UL get body
let ul = document.getElementsByTagName('ul')[0];
console.log(ul.parentNode.parentNode.nodeName);

// given p get 3rd li
let p = document.getElementsByTagName('p')[0];
console.log(p.previousElementSibling.lastElementChild.innerHTML);


