/**
     * Creates a new Car object
     * @constructor
     * @param {String} model
     */
const Car = function(model) {
    this.currentSpeed=0;
    this.model = model;
}

Car.prototype.accelerate = function(){
    this.currentSpeed ++;
}

Car.prototype.break = function(){
    if (this.currentSpeed > 0) {
        this.currentSpeed --;
    }
}

Car.prototype.toString = function(){
    return `${this.model} is going ${this.currentSpeed}`;
}

let car = new Car('taurus');
for(let i = 0;i<=55;i++) {
    car.accelerate();
}
car.break();

console.log(car.toString());

