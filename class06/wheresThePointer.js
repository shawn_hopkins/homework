// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click

$(document).ready(function(){
    $('table').on("click",function (e) {
         e.target.innerText=`${e.clientX}, ${e.clientY}`;
    });
});


