// If an li element is clicked, toggle the class "done" on the <li>
$(document).ready(function(){

  $('a.add-item').on('click', function (e) {
    addListItem(e);
  })

  const list = [{tp:'Later', text:'homework',target:'today-list',move_class:'toLater'},
                {tp:'Today', text:'reading', target:'later-list',move_class:'toToday'}];
  addDefaultListItems(list);

  $( function() {
    $( "ul" ).sortable().disableSelection();
  });
});

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
  e.preventDefault();
  const text = $(e.target).parent().find('input').val();
  if (text==""){
    return;
  }
  let target;
  // Finish function here
  let isLater = ($(e.target).parent().parent().attr('class') == 'later') ? 1 : 0;
  if (isLater) {
    target = $('ul.later-list');
    target.find('a').off();

    target.append(`<li><span>${text}</span><a class="move toLater">Move to Today</a><a class="delete">Delete</a></li>`);
    target.find('a.move').on('click',function (e) {
      moveToToday(e);
    });

  }
  else{
    target = $('ul.today-list');
    target.find('a').off();
    target.append(`<li><span>${text}</span><a class="move toToday">Move to Later</a><a class="delete">Delete</a></li>`);
    target.find('a.move').on('click',function (e) {
      moveToLater(e);
    });
  }

  target.find('a.delete').on('click',function (e) {
    $(e.target).parent().remove();
  });

  target.find('span').last().on('click', function (e) {
    $(e.target).toggleClass('done');
    e.stopPropagation();
  });

  $(e.target).parent().find('input').val("");

}



/*=======================================================
| Add the default items to start
|========================================================*/

const addDefaultListItems = function (items) {

  for (let i of items) {

    let target = $(`ul.${i['target']}`);
    target.append(`<li><span>${i['text']}</span><a class="move ${i['move_class']}">Move to ${i['tp']}</a><a class="delete">Delete</a></li>`);


    if (i['tp'] == 'Later') {
      target.find('a.move').on('click', function (e) {
        moveToLater(e);
      });

    }
    else{
      target.find('a.move').on('click', function (e) {
        moveToToday(e);
      });
    }

    target.find('a.delete').on('click',function (e) {
      $(e.target).parent().remove();
    });

  }
    $("ul").find('li').on('click', function (e) {
    $(e.target).toggleClass('done');
    e.stopPropagation();
  });
}



// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>

// Move item to later
const moveToLater = function (e) {
  const text = $(e.target).parent().find('span')[0].innerHTML;
  const done = $(e.target).parent().find('span').hasClass('done') ? 'class="done"' : "";
  target = $('ul.later-list');
  target.find('a').off();
  target.append(`<li ><span ${done}>${text}</span><a class="move toToday">Move to Today</a><a class="delete">Delete</a></li>`);
  target.find('a.delete').on('click',function (e) {
    $(e.target).parent().remove();
  });

  target.find('a.move').on('click',function (e) {
    moveToToday(e);
  });

  target.find('span').last().on('click', function (e) {
    $(e.target).toggleClass('done');
    e.stopPropagation();
  });

  $(e.target).parent().remove();
}




// Move the item to today
const moveToToday = function (e) {
  const text = $(e.target).parent().find('span')[0].innerHTML;
  const done = $(e.target).parent().find('span').hasClass('done') ? 'class="done"' : "";
  target = $('ul.today-list');
  target.find('a').off();
  target.append(`<li><span ${done}>${text}</span><a class="move toLater">Move to Later</a><a class="delete">Delete</a></li>`);

  target.find('a.delete').on('click',function (e) {
    $(e.target).parent().remove();
  });

  target.find('a.move').on('click',function (e) {
    moveToLater(e);
  });

  target.find('span').last().on('click', function (e) {
    $(e.target).toggleClass('done');
    e.stopPropagation();
  });

  $(e.target).parent().remove();
}




