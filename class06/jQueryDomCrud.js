// Given the HTML page from the class 5 repo (domcrud.
//     html):
// > Create a new <a> element containing the text "Buy
// Now!" with an id of "cta" after the last <p>
// > Access (read) the data-color attribute of the <img>, log
// to the console.
// > Update the third <li> item ("Turbocharged"), set the
// class name to "highlight"
// > Remove (delete) the last paragraph ("Available for
// purchase now…")

// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>

$(document).ready(function(){

    $('p').append("<a id='cta'>Buy now</a>");

    console.log($('img').data('color'));

    $( "li:nth-child(3)" ).addClass('highlight');
});