/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl
 * @param {Event} submitEvent
 */


$(document).ready(function(){
    
    document.getElementById('reason').addEventListener('change',function (e) {
        displayDetails(e);
    });

});

let db = window.localStorage;
db.setItem('contact_type','unknown');

const displayDetails = function(e){
    let reason = e.target;

    let job =document.getElementById('job-reason');
    let code =document.getElementById('code-reason');
    db.setItem('contact_type',reason.value);
    document.getElementById('local').innerHTML=db.getItem('contact_type');

    if (reason.value =='job'){
        job.classList.remove('no-display');

        if (!code.classList.contains('no-display')){
            code.classList.add('no-display');
        }
    }
    else{
        code.classList.remove('no-display');
        if (!job.classList.contains('no-display')) {
            job.classList.add('no-display');
        }
    }
}



const validateItem = function( submitEvent) {

    const inputElements = document.getElementsByClassName('validate-input');
    // submitEvent.preventDefault();

    let isValid=true;


    for (let inputEl of  inputElements) {

        const errorEl = inputEl.parentElement.querySelector('.error');

        if (!inputEl.validity.valid) {
            const labelEl = inputEl.parentElement.querySelector('label');
            if (!inputEl.value) {
                errorEl.innerHTML = `${labelEl.innerText} is Required`;
            } else {
                errorEl.innerHTML = `${labelEl.innerText} is Not Valid`;
            }
            errorEl.classList.add('d-block');
            isValid = false;
            submitEvent.preventDefault();
        } else {
            errorEl.innerHTML = '';
            errorEl.classList.remove('d-block');
        }

        // Length of name must be >=3 chars
        const name = document.getElementById('first-name');
        let errName = name.parentElement.querySelector('small');
        if (name.value.length < 3) {
            const labelEl = name.parentElement.querySelector('label');
            errName.innerHTML = `${labelEl.innerText} must be at least 3 characters`;
            errName.classList.add('d-block');
            isValid = false;
            submitEvent.preventDefault();
        } else {
            errName.innerHTML = '';
            errName.classList.remove('d-block');
        }
    }


    // comment must be 10 chars
    const comment = document.getElementById(    'comment');
    let errComment = comment.parentElement.querySelector('small');
    if (comment.value.length<10) {
        const labelEl = comment.parentElement.querySelector('label');
        errComment.innerHTML = `${labelEl.innerText} must be at least 10 characters`;
        errComment.classList.add('d-block');
        isValid=false;
        submitEvent.preventDefault();
    }
    else {
        errComment.innerHTML = '';
        errComment.classList.remove('d-block');
    }

    // job
    let reason =document.getElementById('reason');
    let jobTitle = document.getElementById('job-title');
    let jobSite = document.getElementById('job-site');
    let errJob = jobTitle.parentElement.querySelectorAll('small');

    if (reason.value =='job') {
        if (jobTitle.value == ""){
            errJob[0].innerHTML = "If 'Job' is selected, you must specify title";
            errJob[0].classList.add('d-block');
            isValid = false;
            submitEvent.preventDefault();
        }
        else{
            errJob[0].innerHTML = "";
            errJob[0].classList.remove('d-block');
        }
        if (!/https?\:\/\/.+\..+/.test(jobSite.value)){
            errJob[1].innerHTML = "Company website is invalid";
            errJob[1].classList.add('d-block');
            isValid = false;
            submitEvent.preventDefault();
        }
        else{
            errJob[1].innerHTML = "";
            errJob[1].classList.remove('d-block');
        }
    }
    // code
    if (reason.value =='code') {
            let errCode = document.getElementById('comp-lang').parentElement.querySelector('small');
            if(parseInt(document.getElementById('comp-lang').selectedIndex) < 1){

                errCode.innerHTML = "If 'Code' is selected, you must specify a language";
                errCode.classList.add('d-block');
                isValid = false;
                submitEvent.preventDefault();
            }
            else {
                errCode.innerHTML = "";
                errCode.classList.remove('d-block');
            }
    }





    let code =document.getElementById('code-reason');
    db.setItem('contact_type',reason.value);
    document.getElementById('local').innerHTML=db.getItem('contact_type');

    if (isValid === false){
        submitEvent.preventDefault();
    }


}



const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
        validateItem(e);

        // for (let i = 0; i < inputElements.length; i++) {
        //     validateItem(inputElements[i], e);
        // }


        // e.preventDefault();
    });

