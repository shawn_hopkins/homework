/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl
 * @param {Event} submitEvent
 */
const validateItem = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');



    if (inputEl.id.indexOf('name')>0) {
        const labelEl = inputEl.parentElement.querySelector('label');
        if (inputEl.value.length < 3) {
            errorEl.innerHTML = `${labelEl.innerText} must be greater than 3 characters`;
            errorEl.classList.add('d-block');
            submitEvent.preventDefault();

        } else {
            errorEl.innerHTML = '';
            errorEl.classList.remove('d-block');
        }
    }
    else {
        const labelEl = inputEl.parentElement.querySelector('label');
        if( !/\w+@\w+\.\w+/.test(inputEl.value)){
            errorEl.innerHTML = `${labelEl.innerText} invalid email`;
            errorEl.classList.add('d-block');
        } else {
            errorEl.innerHTML = '';
            errorEl.classList.remove('d-block');
        }
    }
    submitEvent.preventDefault();
}

const inputElements = document.getElementsByClassName('validate-input');

const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
        for (let i = 0; i < inputElements.length; i++) {
            validateItem(inputElements[i], e);
        }

        e.preventDefault();
    });
