const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';


const url = `${BASE_URL}?q=cars&api-key=${API_KEY}`;

fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);

    let article = responseJson.response.docs[0];
    console.log(article);

    const mainHeadline = article.headline.main;
    document.getElementById('article-title').innerText = mainHeadline;
    document.getElementById('article-snippet').innerHTML=article.snippet;

    let url = article.web_url;
    let link = document.getElementById('article-link');
    link.href=url;

    if (article.multimedia.length > 0) {
      const imgUrl = `https://www.nytimes.com/${article.multimedia[0].url}`;
      document.getElementById('article-img').src = imgUrl;
    }
  });
