
let pallet=255;
const body = document.getElementsByTagName('body')[0];
let darkness = setInterval(function () {

    if (pallet>=0){
        pallet--;
        body.style.backgroundColor=`rgb(${pallet},${pallet},${pallet})`;
    }
    else{
        clearInterval(darkness);
    }
},500
);

//
// "rgb(255, 255, 255);"
// After 0.5 second, background-color: "rgb(254, 254, 254);"