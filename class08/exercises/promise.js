let num = new Promise((resolve, reject) => {
    setTimeout(function () {
        let result = Math.random()*10;

        if (result>1) {
            resolve(result);
        }
        else {
            reject(result);
        }
    },1)
});


num.then(() => {
      console.log(`success ${num}`);
    })
    .catch(() => {
        console.log('fail');
    })
    .then(() => {
        console.log(`complete`);
    });


