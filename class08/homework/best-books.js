// create api-key.js file with const API_KEY="your_api_key" in this same directory to use



const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists';


$(document).ready(function(){
    const form = document.getElementById('books-form')
        .addEventListener('submit',function (e) {
            getBestBooks(e);
            e.preventDefault();
        });
});

const getMDY = function () {

    let d = document.getElementById('date').value;
    let m = document.getElementById('month').value;
    let y = document.getElementById('year').value;

    return `${y}-${m.padStart(2,'0')}-${d.padStart(2,'0')}`;

}

const  getBestBooks  = function(e) {

    let dt = getMDY();
    let url = `${BASE_URL}/${dt}/hardcover-fiction.json?api-key=${API_KEY}`;

    fetch(url)
        .then(function (response) {
            return response.json();
        })
        .then(function (responseJson) {

            let books = [];
            for (let i = 0; i < 5; i++) {
                var book = {
                    "author": responseJson.results.books[i].author,
                    "title": responseJson.results.books[i].title,
                    "cover": responseJson.results.books[i].book_image,
                    "rank": responseJson.results.books[i].rank,
                    "description": responseJson.results.books[i].description
                };
                books.push(book);
            }
            $('#book-epoch').html(dt);
            $('#books-list').empty();
            for (let i of books) {
                $('#books-list').append(`<li><img class="smallimg" src="${i.cover}"</img>${i.title} by ${i.author}<br/><div class="desc">${i.description}</div></li>`);
            }
        })
        .catch(function(responseJson){
            $('#book-epoch').html(`No best-sellers were found for ${dt}` );
            $('#books-list').empty();
        });
}

