const object1 = {desc:"object 1",price: 5.55};
const object2 = {desc:"object 2",price: 6.55};
const object3 = {desc:"object 3",price: 7.55};


const logReciept=function(tax, ... items){
    let sum=0;
    for(let i of items){
        sum += i.price;
        console.log(`${i.desc} - ${i.price}`)
    }
    console.log(`Subtotal: ${sum.toFixed(2)}`);
    console.log(`Total ${(sum + (sum* (tax*.01))).toFixed(2)}`)
}

logReciept(10,object1,object2,object3);