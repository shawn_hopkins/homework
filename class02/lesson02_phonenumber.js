const phoneNumbers =['(206) 333-4444', '206-333-4444', '206 333 4444', ''];

const testPhoneNumber = function (phoneNumber) {
    return /^\(?\d{3}\)?[\s.-]*\d{3}[\s.-]*\d{4}$/.test(phoneNumber)
}

const parsePhoneNumber = function (phone) {
    if (testPhoneNumber(phone)){
        let _phoneNumber=/^\(?(\d{3})\)?[\s.-]*(\d{3})[\s.-]*(\d{4})$/.exec(phone);
        return {areaCode:_phoneNumber[1],phoneNumber: _phoneNumber.slice(2).join('')};
    }
}

for(let i =0;i<phoneNumbers.length;i++){
    console.log(`The number ${phoneNumbers[i]} ${testPhoneNumber(phoneNumbers[i]) ? 'valid':'invalid'}`);
    if (testPhoneNumber(phoneNumbers[i])){
        console.log(`areacode= ${parsePhoneNumber(phoneNumbers[i]).areaCode} number=${parsePhoneNumber(phoneNumbers[i]).phoneNumber}`);
    }
}