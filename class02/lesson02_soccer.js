(function () {

    var Points = {'w':3,'l':0,'d':1}

    const getTotalPoints = function (results) {
        let sum = 0;

        const _results = results.split('');
        _results.forEach(function (result) {
            sum += Points[result];
        });
        return sum;
    }

    const orderResults = function(){
        const args = Array.from(arguments);
        args.forEach(function (team) {
            console.log(`${team['name']} scored ${getTotalPoints(team['results'])} points`);
        });
    }
    
    orderResults({name: 'Sounders', results: 'wwdl'}, {name: 'Galaxy', results: 'wlld'});
})();