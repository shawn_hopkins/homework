const isFoodCooked = function (kind,internalTemp,doneness='done') {
    const foodTemps = {'beef': {'rare':125,'medium':135,'well':155},
                       'chicken':{'done':165}};
    return internalTemp >= foodTemps[kind][doneness] ? true:false;
}

console.log( isFoodCooked('beef',135,'well'));
console.log( isFoodCooked('chicken',170));
