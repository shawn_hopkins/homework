
const getDeck = function () {

    let deck = [];

    const cards = [['2',2], ['3',3], ['4',4], ['5',5], ['6',6], ['7',7], ['8',8], ['9',9], ['10',10], ['Jack',10], ['Queen',10], ['King',10], ['Ace',11]];
    const suits = ['diamonds', 'hearts', 'spades', 'clubs'];

    for (let s of suits) {
        for (let c of cards) {
            let card = {val:c[1],displayVal:c[0],suit:s};
            deck.push(card);
        }
    }

    return deck;
}

for (let c of getDeck()){

    document.write(`${c['displayVal']} of ${c['suit']} is worth ${c['val']} points <br />`);
}